;***
%define POINT 0

%macro colon 2
%ifid %2 
%else
	%error "Error: incorrect name for the label"
%endif
%ifstr %1
%else
	%error "Error: key must be a string"
%endif
%%next: dq POINT
db %1,0
%2:

%define POINT %%next
%endmacro

