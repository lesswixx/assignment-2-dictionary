%include "colon.inc"
%include "words.inc"
%include "lib.inc"
%include "dict.inc"
%define SIZE_OF_BUF 255
%define LINK_SIZE 8

global _start



section .rodata
hel:    db "Enter key: ", 0
rig:    db "Value is: ", 0
e_size: db "Error: max symb number is 255", 0xA, 0
e_name: db "Error: doesn't have that key", 0xA, 0

section .bss

buf: resb SIZE_OF_BUF
;
section .text


_start:
    mov rdi, hel
    call print_string
    mov rdi, buf
    mov rsi, SIZE_OF_BUF
    call read_word
    test rax, rax
    je .s_end
    mov rsi, POINT
    mov rdi, rax
    push rdx
    call find_word
    pop rdx
    test rax, rax
    je .w_end
    add rax, LINK_SIZE
    mov rdi, rax
    push rdi
    mov rdi, rig
    push rdx
    call print_string
    pop rdx
    pop rdi
    add rdi, 1
    add rdi, rdx
    call print_string
    call print_newline
    jmp .end
.w_end:
    mov rdi, e_name
    call print_error
    jmp .end
.s_end:
    mov rdi, e_size
    call print_error
.end:
    call exit
