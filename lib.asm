

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error


section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov  rax, 60
    xor  rdi, rdi          
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину

string_length:


    xor rcx, rcx
.count:
    cmp byte[rdi+rcx], 0
    je .end
    inc rcx
    jmp .count

.end:

    mov rax, rcx

    ret

print_error:
    xor rax, rax

    call string_length

    mov rsi, rdi

    mov rdx, rax

    mov rdi, 2

    mov rax, 1

    syscall

    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout

print_string:



    call string_length

    mov rdx, rax

    mov rax, 1 

    mov rsi, rdi 

    mov rdi, 1 

    syscall

    ret



; Принимает код символа и выводит его в stdout

print_char:

    push rdi

    mov rsi, rsp

    mov rax, 1

    mov rdi, 1 

    mov rdx, 1

    syscall

    pop rdi

    ret



; Переводит строку (выводит символ с кодом 0xA)

print_newline:

    mov rax, 10

    mov rdi, rax

    call print_char

    ret



; Выводит беззнаковое 8-байтовое число в десятичном формате 

; Совет: выделите место в стеке и храните там результаты деления

; Не забудьте перевести цифры в их ASCII коды.

print_uint:

    xor rax, rax 

    mov r9, 10 ;делитель

    mov r10, rsp ; сохраняем указатель

    mov rax, rdi;

    push 0

.cycle:

        xor rdx,rdx 

        

                            ;чтобы избежать переполнения формата - обнуляем

        div r9 

        add rdx, 0x30      ;переводим символ в ASCII

        dec rsp 

        mov byte[rsp], dl ;записываем остаток в стек

        cmp rax,0 

        je .end

        jmp .cycle



.end:

        mov rdi, rsp 

        push r10

        call print_string

        pop r10

        mov rsp, r10 ; 

        ret



; Выводит знаковое 8-байтовое число в десятичном формате 

print_int:

	xor rdx, rdx

	cmp rdi, 0		    

	jns .print_unsign	

	push rdi		    

	mov rdi, '-'

	call print_char		

	pop rdi			

	neg rdi			    

    .print_unsign:

        call print_uint

        ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе

string_equals:

  push rbx

  xor rbx, rbx

    xor rax, rax

  .loop:

    mov cl, byte [rdi+rbx]

    cmp cl, byte [rsi+rbx]

    jnz .not_ok

    cmp byte [rdi + rbx], 0

    jz .ok

    inc rbx

    jmp .loop

  .ok:

    mov rax, 1

    jmp .end

  .not_ok:

    mov rax, 0

    jmp .end

  .end:

    pop rbx

    ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока

read_char:

    push 0			

	mov rdx, 1		

	mov rsi, rsp	

	mov rdi, 0  	

	xor rax, rax	;

	syscall

	pop rax			

 	ret 

    



; Принимает: адрес начала буфера (rdi), размер буфера (rsi)

; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .

; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.

; Останавливается и возвращает 0 если слово слишком большое для буфера

; При успехе возвращает адрес буфера в rax, длину слова в rdx.

; При неудаче возвращает 0 в rax

; Эта функция должна дописывать к слову нуль-терминатор



read_word:

    mov r8, rdi

    mov r9, rsi

    .read_spaces:

     call read_char

        cmp al, 0x20

        je .read_spaces

        cmp al, 0x9

        je .read_spaces

        cmp al, 0xA

        je .read_spaces

    xor rdx, rdx

    .loop:

        cmp al, 0xA

        je .finish

        cmp al, 0x20

        je .finish

        cmp al, 4

        je .finish

        cmp al, 0x9

        je .finish

        cmp al, 0

        je .finish

        inc rdx

        cmp rdx, r9

        jge .overflow

        dec rdx

        mov [r8+rdx], al

        inc rdx

        push rdx

        push r8

        call read_char

        pop r8

        pop rdx

        jmp .loop

    .finish:

        mov byte [r8+rdx], 0

        mov rax, r8

        ret

    .overflow:

        xor rax, rax

        ret





; Принимает указатель на строку, пытается (rdi)

; прочитать из её начала беззнаковое число.

; Возвращает в rax: число, rdx : его длину в символах

; rdx = 0 если число прочитать не удалось



parse_uint:

    xor     r11, r11         

    xor     rdx, rdx       

    xor     rax, rax        

    mov     r10, 0xa       

    .loop:

        xor     r9, r9  

        mov     r9b, [rdi]  

        inc     rdi         

        cmp     r9b, '0'    

        jb      .success

        cmp     r9b, '9'    

        ja      .success

        mul     r10         

        cmp     rdx, 0      

        jne     .error

        sub     r9b, 0x30   

        inc     r11         

   	add     rax, r9    

        jmp     .loop       

    .success:

        mov     rdx, r11   

        ret

    .error:

    	xor     rax, rax

        xor     rdx, rdx

        ret                                    

    



; Принимает указатель на строку, пытается

; прочитать из её начала знаковое число.

; Если есть знак, пробелы между ним и числом не разрешены.

; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 

; rdx = 0 если число прочитать не удалось



parse_int:



    cmp byte[rdi], '-'          

    je .negative                

    cmp byte[rdi], '+'          

    jne parse_uint              

    .positive:                 

        inc rdi                 

        call parse_uint         

        inc rdx                

        ret

    .negative:

        inc rdi                 

        call parse_uint

        neg rax                 

        inc rdx

	ret





; Принимает указатель на строку, указатель на буфер и длину буфера

; Копирует строку в буфер

; Возвращает длину строки если она умещается в буфер, иначе 0



string_copy:

    call string_length              

    cmp rdx, rax                    

    jbe .else                      

    mov rcx, rax                   

    .loop:

        test rcx, rcx               

        je .end                 

        dec rcx                     

        mov dl, byte [rdi + rcx]    

        mov byte [rsi + rcx], dl

        jmp .loop

    .end:

        mov byte [rsi + rax], 0

        ret

    .else:

        xor rax, rax

        ret 




