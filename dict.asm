section .text
global find_word
extern string_equals
%define LINK_SIZE 8
 
find_word:
	xor rax, rax
.loop:
	test rsi, rsi
	jz .notfound
	push rdi
	push rsi

	add rsi, LINK_SIZE
	call string_equals
	pop rsi
	pop rdi
	test rax, rax
	jnz .found
	mov rsi, [rsi]
	jmp .loop
.found:
	mov rax, rsi
.notfound:
	ret