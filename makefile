ASM=nasm
ASMFELF=-f elf64
LD=ld

dict.o: dict.asm
	$(ASM) $(ASMFELF) -o %@ %<

lib.o: lib.asm
	$(ASM) $(ASMFELF) -o %@ %<

main.o: main.asm colon.inc words.inc
	$(ASM) $(ASMFELF) -o %@ %<

main: *.o
	LD -o $@ $<

.PHONY: clean
clean:
	rm *.o
